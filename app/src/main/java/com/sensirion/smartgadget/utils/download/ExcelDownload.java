package com.sensirion.smartgadget.utils.download;

import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.androidplot.xy.SimpleXYSeries;
import com.sensirion.smartgadget.utils.Converter;
import com.sensirion.smartgadget.view.MainActivity;
import com.sensirion.smartgadget.view.history.type.HistoryUnitType;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import static android.content.ContentValues.TAG;

public class ExcelDownload {

    private MainActivity mainActivity;

    public ExcelDownload(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public String downloadExcel(SimpleXYSeries xySeriesTemperature, SimpleXYSeries xySeriesHumidity) {
        final String fileName = String.format("deviceData_%s.xls", getDateFromUnixTimeStamp("" + System.currentTimeMillis()));

        //Saving file in external storage
        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File(sdCard.getAbsolutePath() + "/download");

        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }

        //file path
        File file = new File(directory, fileName);

        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));
        WritableWorkbook workbook;

        try {
            workbook = Workbook.createWorkbook(file, wbSettings);

            //Excel sheet name. 0 represents first sheet
            if (xySeriesHumidity != null) {
                addSheetToWorkbook(xySeriesHumidity, HistoryUnitType.HUMIDITY, workbook);
            } else {
//                Toast.makeText(mainActivity, "Humidity value not found", Toast.LENGTH_SHORT).show();
            }

            if (xySeriesTemperature != null) {
                addSheetToWorkbook(xySeriesTemperature, HistoryUnitType.TEMPERATURE, workbook);
            } else {
//                Toast.makeText(mainActivity, "Temperature value not found", Toast.LENGTH_SHORT).show();
            }

            if (workbook.getSheets().length > 0) {
                workbook.write();
            } else {
//                Toast.makeText(mainActivity, "No sheets found", Toast.LENGTH_SHORT).show();
            }

            try {
                workbook.close();
            } catch (WriteException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            mainActivity.checkAndRequestPermissions(mainActivity);
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }

    private void addSheetToWorkbook(SimpleXYSeries xySeries, HistoryUnitType historyUnitType, WritableWorkbook workbook) {
        WritableSheet sheet = workbook.createSheet(historyUnitType == HistoryUnitType.HUMIDITY ? "Humidity" : "Temperature", 0);

        try {
            sheet.addCell(new Label(0, 0, "Device Id")); // column and row
            sheet.addCell(new Label(1, 0, "Timestamp"));
            sheet.addCell(new Label(2, 0, historyUnitType == HistoryUnitType.HUMIDITY ? "Humidity" : "Temperature"));
            int i = 1;
            StringBuilder valueString = new StringBuilder();
            valueString.append(historyUnitType == HistoryUnitType.HUMIDITY ? "Humidity" : "Temperature");
            for (int x = 0; x < xySeries.size(); x++) {
                String deviceId = xySeries.getTitle();
                String timestamp = getDateFromUnixTimeStamp(String.valueOf(historyUnitType == HistoryUnitType.HUMIDITY ? System.currentTimeMillis() : xySeries.getX(x)));
                String unitTypeValue = String.valueOf(xySeries.getY(x));

                sheet.addCell(new Label(0, i, deviceId));
                sheet.addCell(new Label(1, i, timestamp));
                sheet.addCell(new Label(2, i, unitTypeValue));

                i++;

                valueString.append(i).append("-").append(unitTypeValue).append(":");
            }
//            Toast.makeText(mainActivity, valueString, Toast.LENGTH_SHORT).show();

        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        } catch (Exception e) {
//            Toast.makeText(mainActivity, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private String getDateFromUnixTimeStamp(String timestampString) {
        String formattedDate = "";
        try {
            Long timestamp = Long.parseLong(timestampString);
            Date date = new java.util.Date(timestamp);
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            sdf.setTimeZone(tz);
            formattedDate = sdf.format(date);
        } catch (Exception e) {
            return timestampString;
        }

        return formattedDate;
    }
}
